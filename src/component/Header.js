import React from 'react';
import {Flex, Box} from 'grid-styled';

export const Header = props => {
  return (
    <div>
      <Flex flexWrap="wrap">
        <Box p={3}>
          <h1>{props.companyName}</h1>
        </Box>
      </Flex>
      <Flex flexWrap="wrap">
        <Box width={3 / 4} p={3}>
          <h4>{props.companyMotto}</h4>
        </Box>
        <Box width={1 / 4} p={3}>
          <h4>Since&nbsp;{props.companyEst}</h4>
        </Box>
      </Flex>
    </div>
  );
};

export default Header;
