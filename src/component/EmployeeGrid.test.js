import React from 'react';
import {EmployeeGrid} from './EmployeeGrid';
const ReactTestRenderer = require('react-test-renderer');

describe('EmployeeGrid', () => {
  it('Should compare the component with a snapshot', () => {
    const component = ReactTestRenderer.create(
      <EmployeeGrid employee={{bio: ''}} />,
    );

    const json = component.toJSON();
    expect(json).toMatchSnapshot();
  });
});
