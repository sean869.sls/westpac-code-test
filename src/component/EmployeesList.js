import React, {Component} from 'react';
import {EmployeeGrid} from './EmployeeGrid';
import {Flex, Box} from 'grid-styled';
import styled from 'styled-components';
import Modal from 'react-modal';

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    width: '50%',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
};
const Container = styled(Flex)``;
const Column = styled(Box)`
  border: 2px solid grey;
`;
export class EmployeesList extends Component {
  constructor() {
    super();
    this.state = {
      modalIsOpen: false,
    };
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  openModal(employee) {
    return e => this.setState({modalIsOpen: true, employee});
  }

  closeModal() {
    this.setState({modalIsOpen: false, employee: {}});
  }
  render() {
    const self = this;
    const {employees} = this.props;

    return (
      <Container flexWrap="wrap">
        {employees.map(employee => (
          <Column
            key={employee.id}
            m={3}
            px={3}
            width={0.3}
            onClick={this.openModal(employee)}>
            <EmployeeGrid
              selected={
                this.state.employee && this.state.employee.id === employee.id
              }
              employee={employee}
            />
          </Column>
        ))}

        <Modal
          style={customStyles}
          isOpen={self.state.modalIsOpen}
          onRequestClose={self.closeModal}
          contentLabel="">
          <EmployeeGrid employee={this.state.employee} isModal={true} />
          <button style={{fontSize: '2em'}} onClick={self.closeModal}>
            close
          </button>
        </Modal>
      </Container>
    );
  }
}
