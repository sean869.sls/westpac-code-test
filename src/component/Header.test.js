import React from 'react';
import {Header} from './Header';
const ReactTestRenderer = require('react-test-renderer');

describe('Header', () => {
  it('Should compare the component with a snapshot', () => {
    const component = ReactTestRenderer.create(
      <Header companyName={'abc'} companyMotto={'def'} companyEst={'ghk'} />,
    );

    const json = component.toJSON();
    expect(json).toMatchSnapshot();
  });
});
