import React from 'react';
import {EmployeesList} from './EmployeesList';
const ReactTestRenderer = require('react-test-renderer');

describe('EmployeesList', () => {
  it('Should compare the component with a snapshot', () => {
    const component = ReactTestRenderer.create(
      <EmployeesList
        employees={[
          {
            id: 1,
            bio: '',
          },
        ]}
      />,
    );

    const json = component.toJSON();
    expect(json).toMatchSnapshot();
  });
});
