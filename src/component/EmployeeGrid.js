import React, {Component} from 'react';
import {Flex, Box} from 'grid-styled';

export class EmployeeGrid extends Component {
  render() {
    const {selected, employee, isModal} = this.props;
    let divStyle = {};
    if (selected) {
      divStyle = {
        backgroundColor: 'red',
      };
    }

    const {dateJoined, jobTitle, age} = employee;

    let bio = null;
    if (isModal) {
      bio = employee.bio;
    } else {
      bio = employee.bio
        .split(' ')
        .splice(0, 10)
        .join(' ');
    }

    return (
      <div style={divStyle}>
        <Flex flexWrap="wrap">
          <Box width={1 / 3} p={1}>
            <img src={employee.avatar} />
            {isModal && (
              <div>
                <h5>{jobTitle}</h5>
                <p>{age}</p>
                <p>{dateJoined}</p>
              </div>
            )}
          </Box>
          <Box width={2 / 3} p={1}>
            <h3>
              {employee.firstName} {employee.lastName}
            </h3>
            <p>{bio}</p>
          </Box>
        </Flex>
      </div>
    );
  }
}
