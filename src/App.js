import React, {Component} from 'react';
import './App.css';
import Header from './component/Header';
import {connect} from 'react-redux';
import {EmployeesList} from './component/EmployeesList';
import Modal from 'react-modal';
Modal.setAppElement('#root');

class App extends Component {
  render() {
    const {employees, companyInfo} = this.props.data;
    return (
      <div>
        <Header
          companyName={companyInfo.companyName}
          companyMotto={companyInfo.companyMotto}
          companyEst={companyInfo.companyEst}
        />
        <hr />
        <EmployeesList employees={employees} />
      </div>
    );
  }
}

export default connect(
  state => ({data: state.data}),
  dispatch => ({
    dispatch,
  }),
)(App);
