import {createStore, combineReducers} from 'redux';
import companyData from '../data/sample-data.json';

export default () => {
  const store = createStore(
    f => f,
    {
      data: companyData,
    },
    window.__REDUX_DEVTOOLS_EXTENSION__ &&
      window.__REDUX_DEVTOOLS_EXTENSION__(),
  );

  return store;
};
